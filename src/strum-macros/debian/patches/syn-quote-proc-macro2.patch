--- a/src/enum_discriminants.rs
+++ b/src/enum_discriminants.rs
@@ -2,7 +2,8 @@
 use syn;
 
 use helpers::{
-    extract_list_metas, extract_meta, filter_metas, get_meta_ident, get_meta_list, unique_meta_list,
+    eq_path_str, extract_list_metas, extract_meta, filter_metas, get_meta_ident, get_meta_list,
+    unique_meta_list,
 };
 
 pub fn enum_discriminants_inner(ast: &syn::DeriveInput) -> TokenStream {
@@ -43,7 +44,9 @@
     // Pass through all other attributes
     let pass_though_attributes =
         filter_metas(discriminant_attrs.iter().map(|&m| m), |meta| match meta {
-            syn::Meta::List(ref metalist) => metalist.ident != "derive" && metalist.ident != "name",
+            syn::Meta::List(ref metalist) => {
+                !eq_path_str(&metalist.path, "derive") && !eq_path_str(&metalist.path, "name")
+            }
             _ => true,
         })
         .map(|meta| quote! { #[ #meta ] })
@@ -56,8 +59,8 @@
 
         // Don't copy across the "strum" meta attribute.
         let attrs = variant.attrs.iter().filter(|attr| {
-            attr.interpret_meta().map_or(true, |meta| match meta {
-                syn::Meta::List(ref metalist) => metalist.ident != "strum",
+            attr.parse_meta().ok().map_or(true, |meta| match meta {
+                syn::Meta::List(ref metalist) => !eq_path_str(&metalist.path, "strum"),
                 _ => true,
             })
         });
--- a/src/enum_properties.rs
+++ b/src/enum_properties.rs
@@ -2,18 +2,18 @@
 use syn;
 use syn::Meta;
 
-use helpers::{extract_meta, is_disabled};
+use helpers::{eq_path_str, extract_meta, is_disabled};
 
 fn extract_properties(meta: &[Meta]) -> Vec<(&syn::Ident, &syn::Lit)> {
     use syn::{MetaList, MetaNameValue, NestedMeta};
     meta.iter()
         .filter_map(|meta| match *meta {
             Meta::List(MetaList {
-                ref ident,
+                ref path,
                 ref nested,
                 ..
             }) => {
-                if ident == "strum" {
+                if eq_path_str(path, "strum") {
                     Some(nested)
                 } else {
                     None
@@ -24,11 +24,11 @@
         .flat_map(|prop| prop)
         .filter_map(|prop| match *prop {
             NestedMeta::Meta(Meta::List(MetaList {
-                ref ident,
+                ref path,
                 ref nested,
                 ..
             })) => {
-                if ident == "props" {
+                if eq_path_str(path, "props") {
                     Some(nested)
                 } else {
                     None
@@ -40,8 +40,8 @@
         // Only look at key value pairs
         .filter_map(|prop| match *prop {
             NestedMeta::Meta(Meta::NameValue(MetaNameValue {
-                ref ident, ref lit, ..
-            })) => Some((ident, lit)),
+                ref path, ref lit, ..
+            })) => Some((&path.segments[0].ident, lit)),
             _ => None,
         })
         .collect()
--- a/src/helpers.rs
+++ b/src/helpers.rs
@@ -1,12 +1,12 @@
 use heck::{CamelCase, KebabCase, MixedCase, ShoutySnakeCase, SnakeCase, TitleCase};
-use syn::{Attribute, Ident, Meta, MetaList};
+use syn::{Attribute, Ident, Meta, MetaList, Path};
 
 use case_style::CaseStyle;
 
 pub fn extract_meta(attrs: &[Attribute]) -> Vec<Meta> {
     attrs
         .iter()
-        .filter_map(|attribute| attribute.interpret_meta())
+        .flat_map(|attribute| attribute.parse_meta())
         .collect()
 }
 
@@ -57,7 +57,7 @@
 where
     MetaIt: Iterator<Item = &'meta Meta>,
 {
-    filter_meta_lists(metas, move |metalist| metalist.ident == attr)
+    filter_meta_lists(metas, move |metalist| eq_path_str(&metalist.path, attr))
 }
 
 /// Returns the `MetaList` that matches the given name from the list of `Meta`s, or `None`.
@@ -89,7 +89,7 @@
 /// Returns the `Ident` of the `Meta::Word`, or `None`.
 pub fn get_meta_ident<'meta>(meta: &'meta Meta) -> Option<&'meta Ident> {
     match *meta {
-        Meta::Word(ref ident) => Some(ident),
+        Meta::Path(ref path) => Some(&path.segments[0].ident),
         _ => None,
     }
 }
@@ -100,7 +100,7 @@
         // Get all the attributes with our tag on them.
         .filter_map(|meta| match *meta {
             Meta::List(ref metalist) => {
-                if metalist.ident == attr {
+                if eq_path_str(&metalist.path, attr) {
                     Some(&metalist.nested)
                 } else {
                     None
@@ -112,11 +112,11 @@
         // Get all the inner elements as long as they start with ser.
         .filter_map(|meta| match *meta {
             NestedMeta::Meta(Meta::NameValue(MetaNameValue {
-                ref ident,
+                ref path,
                 lit: Lit::Str(ref s),
                 ..
             })) => {
-                if ident == prop {
+                if eq_path_str(path, prop) {
                     Some(s.value())
                 } else {
                     None
@@ -160,3 +160,13 @@
         ident_string
     }
 }
+
+/// Checks whether the path is equal to the given string.
+///
+/// Returns `true` if they are same.
+///
+/// Note that the given string should be a single path segment.
+/// In other words, it should not be multi-segment path like `a::b::c`.
+pub fn eq_path_str(path: &Path, s: &str) -> bool {
+    path.get_ident().map_or(false, |ident| ident == s)
+}
