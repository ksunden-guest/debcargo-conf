Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: onig_sys
Upstream-Contact:
 Will Speak <will@willspeak.me>
 Ivan Ivashchenko <defuz@me.com>
Source: http://github.com/iwillspeak/rust-onig

Files: *
Copyright:
 2015 Will Speak <will@willspeak.me>
 2015 Ivan Ivashchenko <defuz@me.com>
License: MIT

Files: ./oniguruma/*
Copyright: 2002-2019 K.Kosako <sndgk393@ybb.ne.jp>
           2002-2019 K.Kosako <kkosako0@gmail.com>
License: BSD-2-Clause

Files: ./oniguruma/src/cp1251.c
Copyright: 2006-2019 Byte <byte@mail.kna.ru>
           2006-2019 K.Kosako <sndgk393@ybb.ne.jp>
License: BSD-2-Clause

Files: ./oniguruma/src/gb18030.c
Copyright: 2005-2019 KUBO Takehiro <kubo@jiubao.org>
           2005-2019 K.Kosako <sndgk393@ybb.ne.jp>
License: BSD-2-Clause

Files: debian/*
Copyright:
 2019 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2019 Helen Koike <helen@koikeco.de>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: BSD-2-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
